# README #

Ce projet implémente l'algorithme des tondeuses MowItNow.

Pour le lancer, il suffit d'exécuter l'application via un éditeur Java, ou éventuellement par ligne de commande.
 
Le fichier en entrée, entree.txt se trouve à la racine du projet et est modifiable afin de tester d'autres données.
Après l'exécution, les données en sortie se trouveront affichées dans la console.

Ce projet comporte également des tests unitaires JUnit, permettant de vérifier le bon comportement des méthodes principales. Ceux ci seront exécutés lors du "build" de ce projet Maven.