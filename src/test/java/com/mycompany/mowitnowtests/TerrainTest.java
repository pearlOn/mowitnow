package com.mycompany.mowitnowtests;

import com.mycompany.mowitnow.Terrain;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author perlazana
 */
public class TerrainTest {

    private Terrain terrain1, terrain2;

    public TerrainTest() {

    }

    @Before
    public void initTest() {
        /*Terrain ayant pour coordonnées Sud Ouest et Nord Est respectives (0,0) (5,5)*/
        terrain1 = new Terrain(5, 5);
        /*Terrain ayant pour coordonnées Sud Ouest et Nord Est respectives (-2,-1) (30,12)*/
        terrain2 = new Terrain(-2, -1, 30, 12);
    }

    @After
    public void finTest() {
    }

    @Test
    public void estDansTerrainTest() {
        assertTrue(terrain1.estDansLeTerrain(2, 4));
        assertTrue(terrain1.estDansLeTerrain(1, 5));

        assertTrue(terrain2.estDansLeTerrain(1, 9));
    }

    @Test
    public void pasDansTerrainTest() {
        assertFalse(terrain1.estDansLeTerrain(-1, 5));
        assertFalse(terrain1.estDansLeTerrain(0, 6));
        assertFalse(terrain2.estDansLeTerrain(-3, 12));

    }

}
