package com.mycompany.mowitnowtests;

import com.mycompany.mowitnow.Orientation;
import com.mycompany.mowitnow.Position;
import com.mycompany.mowitnow.Terrain;
import com.mycompany.mowitnow.Tondeuse;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author perlazana
 */
public class TondeuseTest {

    public Tondeuse tondeuse;

    public TondeuseTest() {

    }

    @Before
    public void initTest() {
        tondeuse = new Tondeuse(new Terrain(5, 5), new Position(1, 2), Orientation.N);
        tondeuse.suivreInstructions("GAGAGAGAA");

    }

    @After
    public void finTest() {
        tondeuse = null;

    }

    @Test
    public void orientationTest() {
        assertTrue(Orientation.N.equals(tondeuse.getOrientation()));

    }

    @Test
    public void positionTest() {
        assertEquals(tondeuse.getPosition().getCoordY(), 3);
        assertEquals(tondeuse.getPosition().getCoordX(), 1);

    }

}
