package com.mycompany.mowitnow;

/**
 *
 * @author perlazana
 */
public class Position {

    int coordX;
    int coordY;

    // Position par defaut : l'origine du repere (0,0)
    public Position() {
        this.coordX = 0;
        this.coordY = 0;
    }

    public Position(int coordX, int coordY) {
        this.coordX = coordX;
        this.coordY = coordY;
    }

    // Getter Setter
    public int getCoordX() {
        return this.coordX;
    }

    public void setCoordX(int coordX) {
        this.coordX = coordX;
    }

    public int getCoordY() {
        return this.coordY;
    }

    public void setCoordY(int coordY) {
        this.coordY = coordY;
    }

    /**
     * Methode affichant la position de l'objet courant
     *
     * @return Les coordonnées X Y de l'objet
     */
    @Override
    public String toString() {
        return this.coordX + " " + this.coordY + " ";
    }

}
