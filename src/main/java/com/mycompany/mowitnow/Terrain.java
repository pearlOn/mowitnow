package com.mycompany.mowitnow;

/**
 *
 * @author perlazana
 */
public class Terrain {

    /* Point supérieur droit */
    private Position posNE;
    /* Point inférieur gauche */
    private Position posSO;

    // Par defaut, l'extremite Sud Ouest du terrain est l'origine du repere (0,0)
    public Terrain(int maxX, int maxY) {
        this.posNE = new Position(maxX, maxY);
        this.posSO = new Position();
    }

    public Terrain(int minX, int minY, int maxX, int maxY) {
        this.posNE = new Position(maxX, maxY);
        this.posSO = new Position(minX, minY);
    }

    //Getter Setter
    public Position getPosNE() {
        return posNE;
    }

    public void setPosNE(Position posNE) {
        this.posNE = posNE;
    }

    public Position getPosSO() {
        return posSO;
    }

    public void setPosSO(Position posSO) {
        this.posSO = posSO;
    }

    /**
     * Methode verifiant si un point, defini par une position en parametre, se
     * trouve dans le terrain
     *
     * @param position
     * @return True si la position est dans le terrain, false sinon.
     */
    public boolean estDansLeTerrain(int x, int y) {
        return (x >= this.posSO.getCoordX()
                && x <= this.posNE.getCoordX()
                && y >= this.posSO.getCoordY()
                && y <= this.posNE.getCoordY());
    }

    /**
     * @return Une chaine sous la forme "Dimensions : A x B." avec A et B les
     * coordonnees de l'extremite Nord Est du terrain.
     */
    @Override
    public String toString() {
        return ("Dimensions : " + posNE.getCoordX() + "x" + posNE.getCoordY() + ".");
    }

}
