package com.mycompany.mowitnow;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author perlazana
 */
public class MowApp {

    // Definition d'un Logger pour afficher les Logs
    public static final Logger LOGGER = Logger.getLogger(MowApp.class.getName());

    public static void traitementFichier() {
        FileReader fr = null;
        try {
            // Lecture du fichier
            File f = new File("entree.txt");
            fr = new FileReader(f);
            BufferedReader br = new BufferedReader(fr);
            try {
                //Lecture de la premiere ligne pour initialiser le terrain
                String line = br.readLine();
                Terrain terrain = new Terrain(Character.getNumericValue(line.charAt(0)), Character.getNumericValue(line.charAt(2)));
                LOGGER.log(Level.INFO, "Terrain créé, " + terrain.toString());

                /*Lecture deux lignes par deux lignes : la premiere pour creer une nouvelle tondeuse,
                 la deuxieme pour lui donner une suite d'instructions a realiser*/
                while ((line = br.readLine()) != null) {

                    if (line instanceof String) {
                        String[] tabLine = line.split("\\s");
                        int x = Integer.parseInt(tabLine[0]), y = Integer.parseInt(tabLine[1]);
                        String direction = tabLine[2];
                        Tondeuse tond = new Tondeuse(terrain, new Position(x, y), direction);
                        line = br.readLine();
                        if (line instanceof String) {
                            tond.suivreInstructions(line);
                            System.out.println(tond.toString());
                        }
                    }

                }

                br.close();
                fr.close();
            } catch (NumberFormatException e) {

                LOGGER.log(Level.SEVERE, "Erreur du format dans la chaine");
            } catch (IOException ex) {
                LOGGER.log(Level.SEVERE, "Erreur lors de la lecture : " + ex.getMessage());
            }
        } catch (FileNotFoundException ex) {
            LOGGER.log(Level.SEVERE, "Fichier non trouvé " + ex.getMessage());
        } finally {
            try {
                fr.close();
            } catch (IOException ex) {
                LOGGER.log(Level.SEVERE, "Impossible de fermer le fichier " + ex.getMessage());
            }
        }
    }

    public static void main(String[] args) {

        traitementFichier();
    }

}
