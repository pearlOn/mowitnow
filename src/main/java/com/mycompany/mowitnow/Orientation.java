package com.mycompany.mowitnow;

/**
 *
 * @author perlazana
 */
public enum Orientation {

    N,
    S,
    E,
    O;

    public static Orientation getNORD() {
        return N;
    }

    public static Orientation getSUD() {
        return S;
    }

    public static Orientation getEST() {
        return E;
    }

    public static Orientation getOUEST() {
        return O;
    }
}
