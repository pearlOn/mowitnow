package com.mycompany.mowitnow;

/**
 *
 * @author perlazana
 */
public class Tondeuse {

    Terrain terrain;
    Position position;
    Orientation orientation;

    public Tondeuse(Position position, Terrain terrain) {
        this.position = position;
        this.terrain = terrain;
    }

    public Tondeuse(Terrain terrain, Position position, Orientation orientation) {
        this.terrain = terrain;
        this.position = position;
        this.orientation = orientation;
    }

    public Tondeuse(Terrain terrain, Position position, String orientation) {
        this.terrain = terrain;
        this.position = position;
        this.orientation = Orientation.valueOf(orientation);
    }

    //getter setter
    public Position getPosition() {
        return this.position;
    }

    public Terrain getTerrain() {
        return terrain;
    }

    public void setTerrain(Terrain terrain) {
        this.terrain = terrain;
    }

    public Orientation getOrientation() {
        return orientation;
    }

    public void setOrientation(Orientation orientation) {
        this.orientation = orientation;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Terrain getLimiteTerrain() {
        return terrain;
    }

    public void setLimiteTerrain(Terrain terrain) {
        this.terrain = terrain;
    }

    /**
     * Methode principale prenant en parametre une chaine d'instruction et
     * effectuant les instructions correspondant a chaque lettre.
     *
     * @param chaine
     */
    public void suivreInstructions(String chaine) {

        //Lecture de la chaine sous forme d'un tableau de caracteres
        for (char c : chaine.toCharArray()) {
            switch (c) {
                case 'D':   //Tourner vers la droite
                    tournerDroite();
                    break;

                case 'G':   //Tourner vers la gauche
                    tournerGauche();
                    break;

                case 'A':   // Avancer
                    avancer(terrain);
                    break;

            }
        }
    }

    /**
     * Methode permettant gerant le deplacement de la tondeuse d'une case vers
     * une autre dans une terrain donne:
     * <br>
     * Si la case suivante est libre, la tondeuse s'y deplace. Sinon, la
     * tondeuse ne bouge pas.
     *
     *
     * @param terrain
     */
    public void avancer(Terrain terrain) {
        // System.out.println("Orientation " + orientation.getDirection());

        int x = this.position.coordX, y = this.position.coordY;

        if (this.orientation.equals(Orientation.getNORD())) {
            y++; // Orientation vers le nord : la prochaine case est (x, y+1)
        } else if (this.orientation.equals(Orientation.getEST())) {
            x++; // Orientation vers l'est : la prochaine case est (x+1, y)
        } else if (this.orientation.equals(Orientation.getOUEST())) {
            x--; // Orientation vers l'ouest : la prochaine case est (x-1, y)
        } else if (this.orientation.equals(Orientation.getSUD())) {
            y--; // Orientation vers le sud : la prochaine case est (x, y-1)
        } else {
            System.err.println("Erreur orientation de la tondeuse : " + this.orientation.name());
        }

        if (terrain.estDansLeTerrain(x, y)) {
            this.position.setCoordX(x);
            this.position.setCoordY(y);
        }
    }

    /**
     * Methode retournant la nouvelle direction apres avoir tourne a gauche
     */
    public void tournerGauche() {
        if (this.orientation.equals(Orientation.getNORD())) {
            this.setOrientation(Orientation.getOUEST());
        } else if (this.orientation.equals(Orientation.getEST())) {
            this.setOrientation(Orientation.getNORD());
        } else if (this.orientation.equals(Orientation.getOUEST())) {
            this.setOrientation(Orientation.getSUD());
        } else if (this.orientation.equals(Orientation.getSUD())) {
            this.setOrientation(Orientation.getEST());
        } else {
            System.err.println("Erreur orientation de la tondeuse : " + this.orientation.name());
        }
    }

    /**
     * Methode retournant la nouvelle direction apres avoir tourne a droite
     */
    public void tournerDroite() {
        if (this.orientation.equals(Orientation.getNORD())) {
            this.setOrientation(Orientation.getEST());
        } else if (this.orientation.equals(Orientation.getEST())) {
            this.setOrientation(Orientation.getSUD());
        } else if (this.orientation.equals(Orientation.getOUEST())) {
            this.setOrientation(Orientation.getNORD());
        } else if (this.orientation.equals(Orientation.getSUD())) {
            this.setOrientation(Orientation.getOUEST());
        } else {
            System.err.println("Erreur orientation de la tondeuse : " + this.orientation.name());
        }
    }

    /**
     * Methode affichant les caracteristiques de la tondeuse : ses coordonnes et
     * son orientation
     *
     * @return Une chaine sous la forme : "X Y O" avec X et Y les coordonnees de
     * la tondeuse et O son orientation
     */
    @Override
    public String toString() {
        return this.position.toString() + this.orientation.toString();
    }

}
